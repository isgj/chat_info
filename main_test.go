package main

import (
	"live_info/service"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestWebSocketHandler(t *testing.T) {
	os.Setenv("WEBENV", "DEV")
	us := service.NewUserService(1)
	ts := httptest.NewServer(usersFunc(us))
	defer ts.Close()
	req, err := http.NewRequest("GET", ts.URL+"/live", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Add("Upgrade", "websocket")
	req.Header.Add("Connection", "Upgrade")
	req.Header.Add("Sec-WebSocket-Version", "13")
	req.Header.Add("Sec-WebSocket-Key", "lrDV1UwLKGs5lA8nJmSNGA==")

	client := ts.Client()
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	if resp.StatusCode != http.StatusSwitchingProtocols {
		t.Fatal("didn't upgrade")
	}
}
