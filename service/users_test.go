package service

import (
	"testing"
	"time"
)

func TestUserService(t *testing.T) {
	us := NewUserService(1)
	if us == nil {
		t.Fatal("empty user service")
	}
	us.ticker.Stop()
	firstUser := us.Subcribe()
	for !us.hasUpdate {
		time.Sleep(10 * time.Millisecond)
	}
	us.sendOnline()
	msg := <-firstUser
	if msg.Type != "user" || msg.Online != 1 {
		t.Error("recived wrong data: ", msg)
	}
	secondUser := us.Subcribe()
	for !us.hasUpdate {
		time.Sleep(10 * time.Millisecond)
	}
	if len(us.users) != 2 {
		t.Error("wrong number of users: ", len(us.users))
	}
	us.sendOnline()
	msg = <-firstUser
	if msg.Type != "user" || msg.Online != 2 {
		t.Error("recived wrong data: ", msg)
	}
	msg = <-secondUser
	if msg.Type != "user" || msg.Online != 2 {
		t.Error("recived wrong data: ", msg)
	}
	msg.Type = "text"
	msg.Text = "message"
	us.Send(msg)
	// msg is broadcasted
	newMsg := <-firstUser
	if newMsg.Type != "text" || newMsg.Text != "message" {
		t.Error("recived wrong message: ", newMsg)
	}
	newMsg = <-secondUser
	if newMsg.Type != "text" || newMsg.Text != "message" {
		t.Error("recived wrong message: ", newMsg)
	}
	us.Remove(secondUser)
	for !us.hasUpdate {
		time.Sleep(10 * time.Millisecond)
	}
	us.sendOnline()
	msg = <-firstUser
	if msg.Type != "user" || msg.Online != 1 {
		t.Error("recived wrong data: ", msg)
	}
}
