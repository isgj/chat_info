export type Message = {
    key: string
    username: string;
    text: string;
}
