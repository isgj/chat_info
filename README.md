# Live Chat

Simple web app using websockets ([link](https://chat-info.herokuapp.com/)).
The server is written in Go and the client in Vue.

### Server
The server is written in Go. It's using go modules so if you are using GO 1.11+
you can clone the project where you like. To run the server you should run the command

```bash
WEBENV=DEV go run main.go
```

The `WEBENV` variable is used in development in order to allow websocket
connections from another URL (the front end).

By default it will listen on port `:8080` but you can modify it by
specifying the `PORT` variable

### Client
The client is using Vue.js.

Install the dependencies by running:
```bash
yarn install
```
or
```bash
npm install
```

and run a local server
```bash
yarn serve
```
or
```bash
npm run serve
```
The websocket assumes the server is listening on port `:8080`. You can set
the `VUE_APP_PORT` variable in `.env.local` file ([more info ](https://cli.vuejs.org/guide/mode-and-env.html#using-env-variables-in-client-side-code)) to make it connect to another port.
