package main

import (
	"log"
	"net/http"
	"os"
	"time"

	"live_info/service"

	"github.com/gorilla/websocket"
)

var checkOriginFunc = func() func(r *http.Request) bool {
	env := true
	if os.Getenv("WEBENV") != "DEV" {
		env = false
	}
	return func(r *http.Request) bool {
		if env {
			return true
		}
		for _, origin := range r.Header["Origin"] {
			if origin[8:] == r.Host { // remove "https://"
				return true
			}
		}
		return env
	}
}

func usersFunc(us *service.UserService) http.HandlerFunc {
	upgrader := websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin:     checkOriginFunc(),
	}
	return func(w http.ResponseWriter, r *http.Request) {
		con, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Println(err)
			return
		}
		ticker := time.NewTicker(30 * time.Second)
		defer func() {
			con.Close()
			ticker.Stop()
		}()
		listen := us.Subcribe()
		con.SetCloseHandler(func(i int, s string) error {
			us.Remove(listen)
			return nil
		})
		con.SetPingHandler(func(appData string) error {
			log.Println("received: ", appData)
			return con.WriteMessage(websocket.PongMessage, nil)
		})
		done := make(chan struct{})
		go func() {
			for {
				msg := service.Message{}
				if err := con.ReadJSON(&msg); err != nil {
					log.Println("reading: ", err)
					us.Remove(listen)
					done <- struct{}{}
					break
				}
				us.Send(&msg)
			}
		}()
		for {
			select {
			case <-done:
				return
			case u := <-listen:
				con.SetWriteDeadline(time.Now().Add(10 * time.Second))
				if err := con.WriteJSON(&u); err != nil {
					log.Println("write error: ", err)
					us.Remove(listen)
					return
				}
			case <-ticker.C:
				con.SetWriteDeadline(time.Now().Add(10 * time.Second))
				if err := con.WriteMessage(websocket.PingMessage, nil); err != nil {
					log.Println("ping error: ", err)
					us.Remove(listen)
					return
				}
			}
		}
	}
}

func main() {
	m := http.NewServeMux()
	us := service.NewUserService(5)
	m.Handle("/", http.FileServer(http.Dir("dist")))
	m.HandleFunc("/live", usersFunc(us))
	port := ":8080"
	if p := os.Getenv("PORT"); p != "" {
		port = ":" + p
	}
	var middleware http.HandlerFunc = func(w http.ResponseWriter, r *http.Request) {
		if len(r.Header["X-Forwarded-Proto"]) > 0 && r.Header["X-Forwarded-Proto"][0] == "http" {
			http.Redirect(w, r, "https://"+r.Host+r.URL.String(), http.StatusMovedPermanently)
			return
		}
		m.ServeHTTP(w, r)
	}
	s := http.Server{
		Addr:         port,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
		Handler:      middleware,
	}
	log.Println("Starting server...")
	log.Fatal(s.ListenAndServe())
}
