const is_prod = import.meta.env.PROD;
const host = is_prod ? window.location.host : import.meta.env.VITE_LOCAL_HOST as string;

export const WEBSOCKET_URL = `${is_prod ? 'wss': 'ws'}://${host}/live`;
export const HOST_URL = `${is_prod ? 'https': 'http'}://${host}`;
