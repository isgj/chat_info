package service

import (
	"log"
	"time"

	"github.com/google/uuid"
)

type Message struct {
	Type     string `json:"type,omitempty"`
	Online   int32  `json:"online,omitempty"`
	UserName string `json:"username,omitempty"`
	Text     string `json:"text,omitempty"`
	UUID     string `json:"key,omitempty"`
}

type UserService struct {
	update    chan struct{}
	register  chan chan *Message
	remove    chan chan *Message
	broadcast chan *Message
	hasUpdate bool
	users     map[chan *Message]struct{}
	online    int32
	ticker    *time.Ticker
}

func NewUserService(i int) *UserService {
	us := UserService{
		update:    make(chan struct{}, 5),
		register:  make(chan chan *Message, 5),
		remove:    make(chan chan *Message, 5),
		broadcast: make(chan *Message, 5),
		users:     make(map[chan *Message]struct{}),
		ticker:    time.NewTicker(500 * time.Millisecond),
	}
	go (&us).ListenUpdates()
	return &us
}

func (us *UserService) Subcribe() chan *Message {
	uc := make(chan *Message, 5)
	us.register <- uc
	return uc
}

func (us *UserService) Send(msg *Message) {
	if msg.Type != "text" {
		return
	}
	uuid, err := uuid.NewUUID()
	if err != nil {
		log.Println(err)
		msg.UUID = time.Now().String()
	} else {
		msg.UUID = uuid.String()
	}
	us.broadcast <- msg
}

func (us *UserService) ListenUpdates() {
	for {
		select {
		case <-us.update:
			us.hasUpdate = true
		case <-us.ticker.C:
			if !us.hasUpdate {
				continue
			}
			us.sendOnline()
		case uc := <-us.register:
			us.users[uc] = struct{}{}
			us.online++
			us.update <- struct{}{}
		case key := <-us.remove:
			us.removeKey(key)
		case msg := <-us.broadcast:
			for user := range us.users {
				user <- msg
			}
		}
	}
}

func (us *UserService) sendOnline() {
	msg := &Message{
		Type:   "user",
		Online: us.online,
	}
	for user := range us.users {
		user <- msg
	}
	us.hasUpdate = false
}

func (us *UserService) Remove(key chan *Message) {
	us.remove <- key
}

func (us *UserService) removeKey(key chan *Message) {
	for user := range us.users {
		if user == key {
			close(user)
			delete(us.users, user)
			us.online--
			us.update <- struct{}{}
			break
		}
	}
}
