import { createGlobalState, useLocalStorage } from '@vueuse/core';

export const useSettingsStore = createGlobalState(
    () => useLocalStorage('settings', {
        username: 'Anonymous',
        limit: 50
    }),
);
