module.exports = {
    mode: 'jit',
    purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            animation: {
                'spin-r': 'spin 2s linear infinite reverse',
            }
        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
};
